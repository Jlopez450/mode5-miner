titlescreen:
		move.b #$ff,titleflag
		move.w #$8230,(a3)
		
		lea (bg_title),a5
		lea ($00ff0000),a1
		bsr decompress
		lea ($00ff0000),a5
		move.w #$4600,d4
		move.l #$60000000,(a3)
		bsr vram_loop
		
        move.l #$100,d0         ;first tile
        move.w #$001c,d5
        move.l #$40000003,(a3) ;vram write to $a000	  
		bsr superloop
		
		lea (palette_title),a5
		move.w #$000f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		lea (pallette_ore),a5
		move.w #$0f,d4
		bsr vram_loop
		lea (palette_titletext),a5
		move.w #$0f,d4
		bsr vram_loop
		
		lea (music4)+64,a2
		move.l a2,vgm_start
		move.w #$2300,sr	
titlewait:
		bsr read_controller	
		bsr title_input
		add.w #$01, modifier
		bra titlewait
		
vblank_title: ;vector
		bsr music_driver
		rte		
title_input:
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7
		beq cursor_up
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd,d7
		beq cursor_down
		eor.b #$ff,d3
		andi.b #$f0,d3
		tst.b d3
		bne accept ;ABC or START
		rts
cursor_up:
		cmpi.b #$00,cursorpos
		beq return
		sub.b #$01,cursorpos
		bra move_cursor
		
cursor_down:
		cmpi.b #$03,cursorpos
		beq return
		add.b #$01,cursorpos
		bra move_cursor
		
move_cursor:
		cmpi.b #$00,cursorpos
		beq cursor1
		cmpi.b #$01,cursorpos
		beq cursor2
		cmpi.b #$02,cursorpos
		beq cursor3
		cmpi.b #$03,cursorpos
		beq cursor4
		rts

cursor1:
		move.l #$c0060000,(a3)
		move.w #$0EEE,(a4)
		move.w #$0000,(a4)
		move.w #$0EEE,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		bra unhold
cursor2:
		move.l #$c0060000,(a3)
		move.w #$0000,(a4)
		move.w #$0EEE,(a4)
		move.w #$0EEE,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		bra unhold
cursor3:
		move.l #$c0060000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0EEE,(a4)
		move.w #$0eee,(a4)
		move.w #$0000,(a4)
		bra unhold
cursor4:
		move.l #$c0060000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0EEE,(a4)
		move.w #$0000,(a4)
		move.w #$0eee,(a4)
		bra unhold
		
accept:
		cmpi.b #$00,cursorpos
		beq title_end
		cmpi.b #$01,cursorpos
		beq load_help
		cmpi.b #$02,cursorpos
		beq load_orehelp
		cmpi.b #$03,cursorpos
		beq load_credits
		rts
		
load_help:
		lea (helpscreen),a5
		move.l #$0000c000,d5
		bsr draw_dialogue2
		bsr unhold
		bra textwait
load_orehelp:
		lea (orescreen),a5
		move.l #$0000c000,d5
		bsr draw_dialogue2
		bsr unhold
		bra textwait
load_credits:
		lea (creditscreen),a5
		move.l #$0000c000,d5
		bsr draw_dialogue2
		bsr unhold
		bra textwait	
		
textwait:
		bsr read_controller
		eor.b #$ff,d3
		andi.b #$f0,d3
		tst.b d3
		beq textwait
        move.l #$100,d0         ;first tile
        move.w #$001c,d5
        move.l #$40000003,(a3) ;vram write to $a000	  
		bsr superloop
		bra unhold
		
title_end:
		move.w #$2700,sr
		move.w #$8210,(a3) ;fix layer 
		move.b #$00,titleflag
		bra titlereturn
		
draw_dialogue2:
		move.l d5,d0
		bsr calc_vram
		move.l d0,(a3)
dialogueloop2:	
		move.b (a5)+,d4		
		cmpi.b #$2a,d4	;* (end of line flag)
		beq nextline2		
		cmpi.b #$25,d4	;% (end of block flag)
		beq return
		cmpi.b #$90,d4	;ore
		bge alt_palette	
nvm:
		andi.w #$00ff,d4
		eor.w #$2000,d4
        move.w d4,(a4)		
		bra dialogueloop2		
nextline2:
		add.l #$00000080,d5 ;skip to next line
		bra draw_dialogue2
		
alt_palette:
		cmpi.b #$d0,d4	;ore
		ble nvm
		andi.w #$00ff,d4
		eor.w #$4000,d4
        move.w d4,(a4)		
		bra dialogueloop2		