    include "ramdat.asm"
    include "header.asm"

start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
		bsr clear_regs
		bsr clear_ram
        bsr setup_vdp
		bsr clear_vram
		bsr region_check
		
 		move.w #$100,($A11100)
		move.w #$100,($A11200)   
		
		move.w #$0000,modifier
		move.w #$0999,power
		move.w #$0172,drillframe
		
		move.l #$40000010,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		
		lea (font),a5
		move.l #$40000000,(a3)
		move.w #$7FF,d4
		bsr vram_loop
		
		lea (tiles),a5
		move.w #$4EF,d4		
		move.l #$50000000,(a3)
		bsr vram_loop
		
		bsr titlescreen
titlereturn:		
		move.w #$8134,(a3)

		lea (palettes),a5
		move.w #$3f,d4
		move.l #$c0000000,(a3)
		bsr vram_loop
		
		lea (sky),a5
		lea ($ff0000),a1
		bsr decompress
		lea ($ff0000),a5
		move.w #$1DA0,d4		
		move.l #$70000002,(a3)
		bsr vram_loop	
		
		lea (sprites),a5
		move.w #$05a0,d4
		move.l #$6c000000,(a3)
		;move.l #$60000000,(a3)
		bsr vram_loop
		
		lea (hud),a5
		move.w #$eff,d4		
		move.l #$40000002,(a3)
		bsr vram_loop
		
		bsr map_hud
		bsr set_hud
		
		lea (spritebuffer),a5
		move.w #$00d0,(a5)+
		move.w #$0a01,(a5)+
		move.w #$4160,(a5)+ ;player
		move.w #$0118,(a5)+
		
		move.w #$0000,(a5)+
		move.w #$0a02,(a5)+
		move.w #$4169,(a5)+ ;chute
		move.w #$0000,(a5)+
		
		move.w #$0000,(a5)+
		move.w #$4a00,(a5)+
		move.w #$4172,(a5)+ ;drill
		move.w #$0000,(a5)+
		
		bsr generate_terrain ;dirt
		bsr place_ore
		bsr make_surface	 ;cut a hole in the top
		bsr render
		bsr map_sky ;ToDo: rocks at $FFEECO(where you stop)
		
		move.w #$8174,(a3)		
		lea (music1)+64,a2
		move.l a2,vgm_start
        move.w #$2300, sr    ;enable ints	
		
loop:
		bsr move_stage
		bsr move_stage_up
		bsr music_driver
		move.w #$0000,freetime
		move.b #$ff,vb_flag	
vb_wait:	
		add.w #$01,freetime
		tst vb_flag
		bne vb_wait
		bra loop
		
setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)
        dbf d4,vram_Loop
        rts  
clear_regs:
		moveq #$00000000,d0
		move.l d0,d1
		move.l d0,d2
		move.l d0,d3
		move.l d0,d4
		move.l d0,d5
		move.l d0,d6
		move.l d0,d7
		move.l d0,a0
		move.l d0,a1
		move.l d0,a2
		move.l d0,a3
		move.l d0,a4
		move.l d0,a5
		move.l d0,a6
		rts
		
draw_dialogue:
		move.l d5,d0
		bsr calc_vram
		move.l d0,(a3)
dialogueloop:	
		move.b (a5)+,d4		
		cmpi.b #$2a,d4	;* (end of line flag)
		beq nextline		
		cmpi.b #$25,d4	;% (end of block flag)
		beq return	
		andi.w #$00ff,d4
		eor.w #$E000,d4
        move.w d4,(a4)		
		bra dialogueloop		
nextline:
		add.l #$00000080,d5 ;skip to next line
		bra draw_dialogue
		
map_hud:
        move.l #$e400,d0         ;first tile
        move.w #$005,d5
        move.l #$6b000002,(a3) ;vram write to $a000	   
superloop:
        move.w #$27,d4
maploop:
        move.w d0,(a4)
        add.w #$1,d0
        dbf d4,maploop
        move.w #$17,d4
maploop2:
        move.w #$00,(a4)
        dbf d4,maploop2
        dbf d5,superloop
        rts
		
make_surface:
		lea (level_buffer),a5
		move.w #$0103,d4
surfaceloop:
		move.w #$0000,(a5)+
		dbf d4, surfaceloop
		rts
		
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3
		moveq	#0, d7
		move.b  #$40, ($A10009)	; Set direction
		move.b  #$40, ($A10003)	; TH = 1
		nop
		nop
		move.b  ($A10003), d3	; d3.b = X ; 1 ; C ; B ; R ; L ; D ; U ;
		andi.b	#$3F, d3		; d3.b = 0 ; 0 ; C ; B ; R ; L ; D ; U ;
		move.b	#$0, ($A10003)	; TH = 0
		nop
		nop
		move.b	($A10003), d7	; d7.b = X ; 0 ; S ; A ; 0 ; 0 ; D ; U ;
		andi.b	#$30, d7		; d7.b = 0 ; 0 ; S ; A ; 0 ; 0 ; 0 ; 0 ;
		lsl.b	#$2, d7			; d7.b = S ; A ; 0 ; 0 ; D ; U ; 0 ; 0 ;
		or.b	d7, d3			; d3.b = S ; A ; C ; B ; R ; L ; D ; U ;
		move.b  #$40, ($A10003)	; TH = 1
		nop
		nop
		move.b	#$0, ($A10003)	; TH = 0
		nop
		nop
		move.b	#$40, ($A10003)	; TH = 1
		nop
		nop
		move.b	($A10003), d7	; d7.b = X ; X ; X ; X ; M ; X ; Y ; Z
		move.b  #$0, ($A10003)	; TH = 0
		andi.w	#$F, d7			; d7.b = 0 ; 0 ; 0 ; 0 ; M ; X ; Y ; Z
		lsl.w	#$8, d7			; d7.w = 0 ; 0 ; 0 ; 0 ; M ; X ; Y ; Z ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ; 0 ;
		or.w	d7, d3			; d3.w = 0 ; 0 ; 0 ; 0 ; M ; X ; Y ; Z ; S ; A ; C ; B ; R ; L ; D ; U ;
		move.w d3,input
		rts	
		
render:
		lea (level_buffer),a5
		move.l #$40000001,(a3)
		move.w #$1f,d5	;depth
render2:
		move.w #$27,d4  ;width
renderloop:
		move.b (a5)+,d1
		cmpi.b #$90,d1
		bge ore
		move.w d1,(a4)
renderret:
		dbf d4, renderloop
		move.w #$0017,d4
emptyloop:
		move.w #$0000,(a4)
		dbf d4,emptyloop
		dbf d5, render2
		rts
ore:
		add.w #$2000,d1 ;palette 2
		move.w d1,(a4)
		clr d1
		bra renderret
		
random_number:
		move.w random,d0
		sub.w d4,d0
		rol.w #$02,d0
		move.w modifier,d1
		eor.w d1,d0
		move.w d0,random	
		rts
		
random_number2:
		move.b random2,d0
		sub.w d4,d0
		rol.w #$02,d0
		move.w modifier,d1 ;more random
		eor.w d1,d0
		move.b d0,random2		
		rts
	
		
clear_ram:
		move.w #$7FF0,d0
		move.l #$FF0000,a0	
clear_ram_loop:
		move.w #$0000,(a0)+
		dbf d0, clear_ram_loop
		rts
clear_vram:
		move.l #$40000000,(a3)
		move.w #$7fff,d4
vram_clear_loop:
		move.w #$0000,(a4)
		dbf d4, vram_clear_loop
		rts		
		
generate_terrain:
		lea (level_buffer),a5
		move.w #$FDFF,d4
terrain_loop:
		bsr random_number2
		move.b random2, d1
		lsr.b #$04,d1
		eor.b #$80,d1
		move.b d1, (a5)+
		dbf d4, terrain_loop
		rts
		
map_sky:
		lea (mapdata_sky),a5
		move.w #$33f,d4
		; move.l #$40000001,(a3) ;layer A
		move.l #$60000002,(a3) ;layer B
skyloop:
		move.w (a5)+,d0
		add.w #$6580,d0
		move.w d0,(a4)
		dbf d4,skyloop
		rts
		
 dc.b "blah"
place_ore:
		lea (level_buffer)+521,a5 ;521 to place top left
		;bra place_iron ;(used to?) make shit not work
		move.w #$ffff,d4 ;random generator depends on D4 for randomness
ore_loop:
		bsr random_number
		move.w random, d1
		lsr.w #$07,d1 ;overall ore density lower = less ore

		cmpi.w #$1,d1
		beq place_iron
		cmpi.w #$17,d1
		beq place_iron
		cmpi.w #$2d,d1
		beq place_iron
		cmpi.w #$43,d1
		beq place_iron
		cmpi.w #$59,d1
		beq place_iron
		cmpi.w #$6f,d1
		beq place_iron
		
		cmpi.w #$85,d1
		beq place_copper
		cmpi.w #$9b,d1
		beq place_copper
		cmpi.w #$b1,d1
		beq place_copper
		cmpi.w #$c7,d1
		beq place_copper
		cmpi.w #$dd,d1
		beq place_copper
		
		cmpi.w #$f3,d1
		beq place_silver
		cmpi.w #$109,d1
		beq place_silver
		cmpi.w #$11f,d1
		beq place_silver
		cmpi.w #$135,d1
		beq place_silver
		
		cmpi.w #$14b,d1
		beq place_gold
		cmpi.w #$161,d1
		beq place_gold
		cmpi.w #$177,d1
		beq place_gold
		
		cmpi.w #$18d,d1
		beq place_emerald
		cmpi.w #$1a3,d1
		beq place_emerald
		
		cmpi.w #$1b9,d1
		beq place_darkmatter
		
		cmpi.w #$1cf,d1
		beq place_battery
		
		;rts
		;add.l #$1,a5
oreret:
		bsr incrament
		;cmpi.l #$FFFE00,a5
		cmpi.l #$FFF000,a5
		bge return
		dbf d4, ore_loop
		rts	
		
incrament:
		add.w #$03,a5	
		add.w #$03,orecounter
		cmpi.w #$27,orecounter
		beq newline
		rts
newline:
		move.w #$0000,orecounter
		add.w #$51,a5
		rts
		
place_iron:
		move.b #$90,(a5)+
		move.b #$91,(a5)+
		move.b #$92,(a5)+
		add.l #$25,a5
		move.b #$93,(a5)+
		move.b #$94,(a5)+
		move.b #$95,(a5)+
		add.l #$25,a5
		move.b #$96,(a5)+
		move.b #$97,(a5)+
		move.b #$98,(a5)+
		sub.l #$53,a5
		bra oreret
place_copper:
		cmpi.l #$ff1200,a5	;minimum spawn depth (100)
		ble oreret
		move.b #$a2,(a5)+
		move.b #$a3,(a5)+
		move.b #$a4,(a5)+
		add.l #$25,a5
		move.b #$a5,(a5)+
		move.b #$a6,(a5)+
		move.b #$a7,(a5)+
		add.l #$25,a5
		move.b #$a8,(a5)+
		move.b #$a9,(a5)+
		move.b #$aa,(a5)+
		sub.l #$53,a5
		bra oreret
place_silver:
		cmpi.l #$ff2900,a5	;minimum spawn depth (250)
		ble oreret
		move.b #$99,(a5)+
		move.b #$9a,(a5)+
		move.b #$9b,(a5)+
		add.l #$25,a5
		move.b #$9c,(a5)+
		move.b #$9d,(a5)+
		move.b #$9e,(a5)+
		add.l #$25,a5
		move.b #$9f,(a5)+
		move.b #$a0,(a5)+
		move.b #$a1,(a5)+
		sub.l #$53,a5
		bra oreret
place_gold:
		cmpi.l #$ff5000,a5	;minimum spawn depth (500)
		ble oreret
		move.b #$ab,(a5)+
		move.b #$ac,(a5)+
		move.b #$ad,(a5)+
		add.l #$25,a5
		move.b #$ae,(a5)+
		move.b #$af,(a5)+
		move.b #$b0,(a5)+
		add.l #$25,a5
		move.b #$b1,(a5)+
		move.b #$b2,(a5)+
		move.b #$b3,(a5)+
		sub.l #$53,a5
		bra oreret
		
place_emerald:
		cmpi.l #$ff774c,a5	;minimum spawn depth (750)
		ble oreret
		move.b #$b4,(a5)+
		move.b #$b5,(a5)+
		move.b #$b6,(a5)+
		add.l #$25,a5
		move.b #$b7,(a5)+
		move.b #$b8,(a5)+
		move.b #$b9,(a5)+
		add.l #$25,a5
		move.b #$ba,(a5)+
		move.b #$bb,(a5)+
		move.b #$bc,(a5)+
		sub.l #$53,a5
		bra oreret

place_darkmatter:
		cmpi.l #$ffc500,a5	;minimum spawn depth (1250
		ble oreret
		move.b #$bd,(a5)+
		move.b #$be,(a5)+
		move.b #$bf,(a5)+
		add.l #$25,a5
		move.b #$c0,(a5)+
		move.b #$c1,(a5)+
		move.b #$c2,(a5)+
		add.l #$25,a5
		move.b #$c3,(a5)+
		move.b #$c4,(a5)+
		move.b #$c5,(a5)+
		sub.l #$53,a5
		bra oreret
		
place_battery:
		eor.b #$ff,inverter
		tst.b inverter
		beq oreret
		move.b #$c6,(a5)+
		move.b #$c7,(a5)+
		move.b #$c8,(a5)+
		add.l #$25,a5
		move.b #$c9,(a5)+
		move.b #$ca,(a5)+
		move.b #$cb,(a5)+
		add.l #$25,a5
		move.b #$cc,(a5)+
		move.b #$cd,(a5)+
		move.b #$ce,(a5)+
		sub.l #$53,a5
		bra oreret
		
return:
		rts

HBlank:
		move.w #$8AFF,(a3)
		lea (pallette_hud),a5;To:Do: skip if depth is high enough
		move.w #$0f,d4
		move.l #$c0600000,(a3)
		bsr vram_loop
		rte

VBlank:
		cmpi.b #$ff,titleflag
		beq vblank_title
		movem.l d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a5/a6, -(sp)
		;add.w #$01,modifier
		bsr dma_start
		; bsr random_number
		; bsr random_number2
		move.w #$8AB0,(a3)
		lea (pallette_bg),a5 
		move.w #$0f,d4
		move.l #$c0600000,(a3)
		bsr vram_loop
		bsr cycle_palette
		bsr spin_drill	
		bsr read_controller	
		bsr test_input
		bsr gravity	
		bsr move_player	
		bsr update_sprites
		bsr clock
		bsr bleed_power
		bsr draw_hud
		move.b #$00,vb_flag
		movem.l (sp)+, d0/d1/d2/d3/d4/d5/d6/d7/a0/a1/a2/a5/a6
        rte
		
cycle_palette:
		add.b #$01,cyctimer
		cmpi.b #$1e,cyctimer
		bne return
		move.b #$00,cyctimer	
ret0:
		add.b #$01,cycnum
		cmpi.b #$04,cycnum
		bge resetcyc	
		cmpi.b #$00,cycnum
		beq setpal1
		cmpi.b #$01,cycnum
		beq setpal2		
		cmpi.b #$02,cycnum
		beq setpal3				
resetcyc:
		move.b #$ff,cycnum
		bra ret0	
setpal1:
		move.l #$c0500000,(a3)
		lea (pcyc1),a5
		move.w #$02,d4
		bsr vram_loop
		rts
setpal2:
		move.l #$c0500000,(a3)
		lea (pcyc2),a5
		move.w #$02,d4
		bsr vram_loop
		rts
setpal3:
		move.l #$c0500000,(a3)
		lea (pcyc3),a5
		move.w #$02,d4
		bsr vram_loop
		rts
		
spin_drill:
		eor.b #$ff,inverter
		tst.b inverter
		beq return
		add.w #$0009, drillframe
		cmpi.w #$0196,drillframe
		bne return
		move.w #$0172,drillframe
		rts
		
bleed_power:
		add.b #$01,power_timer
		cmpi.b #$ff,power_timer
		bne return
		clr power_timer
		bsr subpower
		rts
		
power_drill:
		add.b #$01,power_timer2
		cmpi.b #$20,power_timer2
		ble return
		clr power_timer2
		bsr subpower
		rts
		
gravity:
		cmpi.w #$00,depth
		beq return
		bsr check_bottom
		cmpi.b #$01,d5 ;ToDo: fix
		beq override
		; cmpi.b #$00,d5 ;if nothing below
		; bne nograv
		bra nograv
override:
		cmpi.b #$00,distance
		bne return
		cmpi.w #$01,depth
		beq nograv
		
		
		move.b #$02,direction
		move.b #$18,distance
		move.b #$ff,falling
		rts
		
		move.l #$40000010,(a3)
		add.w #$03,yscroll
		move.w yscroll,(a4)
		rts
		;bsr scroll_down
		;rts	
		
fallfast:
		lea (spritebuffer),a5
		add.w #$02,(a5)	;fall speed		
nograv:
		lea (spritebuffer)+8,a5
		move.w #$0000,(a5)+ ;no chutes!
		move.b #$00,falling
		rts
		
check_bottom:
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0078,d0
		sub.w #$0068,d1
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		move.b d5,block
		move.w a5, area
		rts
		
check_top:
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0078,d0
		sub.w #$0088,d1
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		rts
		
check_right:
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0068,d0
		sub.w #$0078,d1
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		move.b d5,block2
		rts
		
check_left:
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0088,d0
		sub.w #$0078,d1
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b (a5),d5	
		move.b d5,block2
		rts
		
draw_hud:
		move.b minutes,d2
		move.l #$6ba60002,(a3)
		bsr writebyte
		
		move.b seconds,d2
		move.l #$6bac0002,(a3)
		bsr writebyte

		move.w depthBCD,d2
		move.l #$6c140002,(a3)
		bsr writetrip
		
		move.w power,d2
		move.l #$6c940002,(a3)
		bsr writetrip
		rts
		move.w yscroll,d2
		move.l #$60820002,(a3)
		bsr writeword
		
		move.w freetime,d2
		move.l #$61020002,(a3)
		bsr writeword
		
		move.w block,d2	
		move.l #$61820002,(a3)
		bsr writeword
		
		lea (spritebuffer),a5
		move.w (a5),d2	
		move.l #$62020002,(a3)
		bsr writeword
		lea (spritebuffer)+6,a5
		move.w (a5),d2	
		move.l #$62820002,(a3)
		bsr writeword	
		move.w area,d2	
		move.l #$63020002,(a3)
		bsr writeword
		move.w modifier,d2	
		move.l #$63820002,(a3)
		bsr writeword
		move.w depth,d2	
		move.l #$64020002,(a3)
		bsr writeword
		move.w block2,d2	
		move.l #$64820002,(a3)
		bsr writeword
		move.w input,d2	
		move.l #$65020002,(a3)
		bsr writeword
		rts
		
set_hud:
		move.l #$6b9c0002,(a3)
		lea hudtext1,a5
		bsr termtextloop
		move.l #$6c080002,(a3)
		lea hudtext2,a5
		bsr termtextloop
		move.l #$6c880002,(a3)
		lea hudtext3,a5
		bsr termtextloop
		rts
		
termtextloop:					;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$24,d4			;$ (end of text flag)
		beq return
		andi.w #$00ff,d4
		eor.w #$e000,d4
        move.w d4,(a4)		
		bra termtextloop
		
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
		
calc_vram_read:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		;eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
			
move_stage_up:
		cmpi.w #$FFF8,yscroll
		bgt return
		bsr subdepth
		bra calc1
move_stage: ;down
		;cmpi.w #$0010,yscroll
		cmpi.w #$0008,yscroll ;changed to 8 to ease depth calculation and GFX errors
		blt return
		bsr adddepth
calc1:		
		moveq.l #$00000000,d2
		move.w depth,d2
		move.w d2,d3
		lsl.w #$05,d2	;faster than MUL by 27
		lsl.w #$03,d3
		; mulu.w #$27,d2
		
		add.w d3,d2
		lea (level_buffer),a5		
		add.l d2,a5
		lea (dma_buffer),a6
		moveq.l #$00000000,d1	
		
		move.w #$1a,d5
dmarender2:
		move.w #$27,d4  ;width
dmarenderloop:
		move.b (a5)+,d1
		cmpi.b #$90,d1
		bge dmaore
		move.w d1,(a6)+
dmarenderret:
		dbf d4, dmarenderloop
		move.w #$0017,d4
dmaemptyloop:
		move.w #$0000,(a6)+
		dbf d4,dmaemptyloop
		dbf d5, dmarender2
		move.b #$ff,dma_flag
		rts
dmaore:
		add.w #$2000,d1 ;palette 2
		move.w d1,(a6)+
		clr d1
		bra dmarenderret
		
dma_start:		
		tst.b dma_flag
		beq return
		move.b #$00,dma_flag	
		move.l #$93809406, (A3) ;DMA length ($0680)
		move.w #$9540,(a3)      ;80
		move.w #$96f8,(a3)      ;f0
		move.w #$977f,(a3)      ;ff		
		move.l #$40000081,(a3);dma $4000
		move.w #$0000,yscroll
		move.l #$40000010,(a3)
		move.w yscroll,(a4)	
		rts
		
adddepth:
		add.w #$01,depth
		move.w #$01,d0
		move.w depthBCD,d1
		cmpi.b #$99,d1
		beq centi
		abcd d0,d1
		move.w d1,depthBCD
		rts
centi:
		add.w #$0067,d1
		move.w d1,depthBCD
		rts
		
subdepth:
		sub.w #$01,depth
		move.w #$01,d0
		move.w depthBCD,d1
		cmpi.b #$00,d1
		beq centi2
		sbcd d0,d1
		move.w d1,depthBCD
		rts
centi2:
		sub.w #$0067,d1
		move.w d1,depthBCD
		rts
		
addpower:
		move.w #$01,d0
		move.w power,d1
		cmpi.b #$99,d1
		beq centi3
		abcd d0,d1
		move.w d1,power
		rts
centi3:
		add.w #$0067,d1
		move.w d1,power
		rts
		
subpower:
		move.w #$01,d0
		move.w power,d1
		cmpi.b #$00,d1
		beq centi4
		sbcd d0,d1
		move.w d1,power
		rts
centi4:
		sub.w #$0067,d1
		move.w d1,power
		rts
		

update_sprites:
		lea (spritebuffer),a5
		move.l #$78000003,(a3)
		move.w #$10,d4
spriteloop:
		move.w (a5)+,(a4)
		dbf d4,spriteloop
		rts
		
test_input:
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe,d7
		beq press_up
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd,d7
		beq press_down
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb,d7
		beq press_left
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7,d7
		beq press_right
		
		move.b d3,d7
		or.b #$7f,d7
		cmpi.b #$7f,d7
		beq pause
		rts
		
pause:
		move.w #$8218,(a3)
		move.w #$8403,(a3)
		lea (pausescreen),a5
		move.l #$00006000,d5
		bsr draw_dialogue
		move.b #$9f,$c00011
		move.b #$EF,$c00011	;kill psg
        move.b #$FF,$c00011		
        move.b #$BF,$c00011
		
		lea (palette_titletext),a5
		move.w #$0f,d4
		move.l #$c0600000,(a3)
		bsr vram_loop
		
		move.l cash,d0
		lea bcdout,a1
		bsr convert
		move.l d0,cashBCD
		swap d0		
		move.w d0,d2
		move.l #$6c220001,(a3)
		bsr writeword
		swap d0
		move.w d0,d2
		move.l #$6c2a0001,(a3)
		bsr writeword
		
		move.w totaliron,d7
		bsr hex2dec
		move.w d7,d2
		move.l #$659c0001,(a3)
		bsr writeword
		move.w totalcopper,d7
		bsr hex2dec
		move.w d7,d2
		move.l #$661c0001,(a3)
		bsr writeword
		move.w totalsilver,d7
		bsr hex2dec
		move.w d7,d2
		move.l #$669c0001,(a3)
		bsr writeword
		move.w totalgold,d7
		bsr hex2dec
		move.w d7,d2
		move.l #$671c0001,(a3)
		bsr writeword
		move.w totalemerald,d7
		bsr hex2dec
		move.w d7,d2
		move.l #$679c0001,(a3)
		bsr writeword
		move.w totaldarkmatter,d7
		bsr hex2dec
		move.w d7,d2
		move.l #$681c0001,(a3)
		bsr writeword
			
		move.w #$2700,sr
		bsr unhold
startloop:
		bsr read_controller
		cmpi.b #$7f,d3
		bne startloop
		bsr unhold
		move.w #$8210,(a3)  
		move.w #$8405,(a3)  	
		move.w #$2300,sr
		rts
		
unhold:	;6 button issue: Reads button (okay), then clear(okay), then button again? Dafuq?
		bsr read_controller
		cmpi.w #$fff,d3
		 beq return
		bra unhold
		
press_up:
		bsr check_top
		cmpi.b #$01,d5
		beq return		;ToDo: check 1 cell higher and continue if not empty
		tst.b distance
		bne return
		move.b #$01,direction
		move.b #$18,distance
		bsr play_drill
		rts
press_down:	
		tst.b distance
		bne return
		cmpi.w #$05EB,depth ;maximum depth
		bge return
		move.b #$02,direction
		move.b #$18,distance
		bsr play_drill
		rts
press_left:
		cmpi.b #$00,falling
		beq skip0
		bsr check_left
		cmpi.b #$01,d5 ;don't move left or right if no ground to dig through
		beq return
skip0: ;if not falling
		tst.b distance
		bne return
		bsr gravity ;ToDo: may cause unforeseen bullshit
		move.b #$03,direction
		move.b #$18,distance
		bsr play_drill
		rts
		
press_right:
		cmpi.b #$00,falling
		beq skip1
		bsr check_right
		cmpi.b #$01,d5
		beq return
skip1:
		tst.b distance
		bne return
		bsr gravity
		move.b #$04,direction
		move.b #$18,distance
		bsr play_drill
		rts
	
scroll_down:
		move.l #$40000010,(a3)
		add.w #$01,yscroll
		move.w yscroll,(a4)
		rts
		
scroll_up:
		cmpi.w #$0000,depth
		beq return
		move.l #$40000010,(a3)
		sub.w #$01,yscroll
		move.w yscroll,(a4)
		rts
		
move_player:
		cmpi.b #$01,direction
		 beq moveup
		cmpi.b #$02,direction
		 beq movedown
		cmpi.b #$03,direction
		 beq moveleft
		cmpi.b #$04,direction
		 beq moveright
		rts
		
moveup:
		tst.b distance
		beq nodrill
		bsr deploy_drill_up
		
		bsr check_top
		bsr check_ore			
		
		bsr tunnel_up
		sub.b #$01,distance
		bra scroll_up
movedown:
		tst.b distance
		beq nodrill
		cmpi.b #$ff,falling
		beq falldown
		bsr deploy_drill_down

		bsr check_bottom
		bsr check_ore		
		
		bsr tunnel_down		
		sub.b #$01,distance
		bra scroll_down	
falldown:
		lea (spritebuffer)+16,a5
		move.w #$0000,(a5) ;no drill
		lea (spritebuffer),a5
		move.w (a5),d0
		sub.w #$18,d0	;position chute
		lea (spritebuffer)+6,a5
		move.w (a5),d1			
		lea (spritebuffer)+8,a5
		move.w d0,(a5)+
		move.w #$0a02,(a5)+	;deploy chutes!
		move.w #$4169,(a5)+
		move.w d1,(a5)+
		sub.b #$01,distance
		bra scroll_down	
		; sub.b #$3,distance
		; move.l #$40000010,(a3)
		; add.w #$3,yscroll
		; move.w yscroll,(a4)
		; rts
moveleft:
		lea (spritebuffer)+6,a5
		cmpi.w #$0088,(a5)
		ble nomove
		tst.b distance
		beq nodrill
		bsr deploy_drill_left
		
		bsr check_left
		bsr check_ore			
		
		bsr tunnel_left
		sub.b #$01,distance
		lea (spritebuffer)+6,a5
		sub.w #$01,(a5)
		rts
moveright:
		lea (spritebuffer)+6,a5
		cmpi.w #$01a8,(a5)
		bge nomove
		tst.b distance
		beq nodrill
		bsr deploy_drill_right
		
		bsr check_right
		bsr check_ore	
		
		bsr tunnel_right
		sub.b #$01,distance
		lea (spritebuffer)+6,a5
		add.w #$01,(a5)
		rts
		
nodrill:
		lea (spritebuffer)+16,a5
		move.w #$0000,(a5)
		rts
		
nomove:
		move.b #$00,distance
		rts
		
deploy_drill_up:
		lea (spritebuffer),a5
		move.w (a5),d0
		sub.w #$14,d0	;position
		lea (spritebuffer)+6,a5
		move.w (a5),d1	
		lea (spritebuffer)+16,a5
		move.w d0,(a5)+
		move.w #$4a00,(a5)+	;deploy drill!
		
		move.w drillframe,d0 
		add.w #$5000,d0
		move.w d0,(a5)+
		
		move.w d1,(a5)+
		rts
		
deploy_drill_down:
		lea (spritebuffer),a5
		move.w (a5),d0
		add.w #$14,d0	;position
		lea (spritebuffer)+6,a5
		move.w (a5),d1	
		lea (spritebuffer)+16,a5
		move.w d0,(a5)+
		move.w #$4a00,(a5)+	;deploy drill!
		move.w drillframe,d0 
		add.w #$4000,d0
		move.w d0,(a5)+
		move.w d1,(a5)+
		rts
		
deploy_drill_left:
		lea (spritebuffer)+8,a5
		move.w #$0000,(a5) ;no chute
		lea (spritebuffer),a5
		move.w (a5),d0
		lea (spritebuffer)+6,a5
		move.w (a5),d1	
		sub.w #$14,d1	;position
		lea (spritebuffer)+16,a5
		move.w d0,(a5)+
		move.w #$4a00,(a5)+	;deploy drill!
		move.w drillframe,d0 
		add.w #$4024,d0
		move.w d0,(a5)+
		move.w d1,(a5)+
		rts
		
deploy_drill_right:
		lea (spritebuffer)+8,a5
		move.w #$0000,(a5) ;no chute
		lea (spritebuffer),a5
		move.w (a5),d0
		lea (spritebuffer)+6,a5
		move.w (a5),d1	
		add.w #$14,d1	;position
		lea (spritebuffer)+16,a5
		move.w d0,(a5)+
		move.w #$4a00,(a5)+	;deploy drill!
		;move.w #$4936,(a5)+
		
		move.w drillframe,d0 
		add.w #$4824,d0
		move.w d0,(a5)+
		
		move.w d1,(a5)+
		rts
		
tunnel_down:
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0080,d0
		sub.w #$0068,d1		
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b #$01,(a5)+
		move.b #$01,(a5)+	
		move.b #$01,(a5)+
		bra power_drill
		
tunnel_up:
		cmpi.w #$0003,depth	;only losers cut holes in the sky
		ble return
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0080,d0
		sub.w #$0088,d1		
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b #$01,(a5)+
		move.b #$01,(a5)+	
		move.b #$01,(a5)+
		bra power_drill
		
tunnel_right:
		cmpi.w #$0000,depth
		beq return
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0068,d0
		sub.w #$0080,d1		
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b #$01,(a5)+
		add.l #$27,a5
		move.b #$01,(a5)+
		add.l #$27,a5		
		move.b #$01,(a5)+
		bsr power_drill
		bra calc1 ;DMA band-aid

tunnel_left:
		cmpi.w #$0000,depth
		beq return
		lea (spritebuffer),a5
		move.w (a5),d1   ;Y pos is now in d1	
		lea (spritebuffer+6),a5
		move.w (a5),d0   ;x pos is now in d0
		andi.w #$0ff8,d1
		andi.w #$0ff8,d0
		sub.w #$0080,d0
		sub.w #$0080,d1		
		move.w depth,d2
		lsl.w #$03,d2
		add.w d2,d1
		mulu.w #$05,d1
		lsr.w #$03,d0
		add.w d0,d1
		lea (level_buffer),a5
		add.l d1,a5
		move.b #$01,(a5)+
		add.l #$27,a5
		move.b #$01,(a5)+
		add.l #$27,a5		
		move.b #$01,(a5)+
		bsr power_drill
		bra calc1 ;DMA band-aid				
		
writeword:
	    move.w d2,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$e010,d5
		move.w d5,(a4)
writetrip:
	    move.w d2,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$e010,d5
		move.w d5,(a4)
writebyte:  ;not to be confused with litebrite
	    move.w d2,d5
		andi.w #$00f0,d5
		lsr #$4,d5
    	add.w #$e010,d5
		move.w d5,(a4)
	    move.w d2,d5
		andi.w #$000f,d5
    	add.w #$e010,d5	
		move.w d5,(a4)		
		rts
		
hex2dec:		
		cmpi.w #$0000,d7
		beq return
		move.w d0, -(sp)
		move.w d1, -(sp)	;push registers
		move.w d3, -(sp)
		move.w #$0001,d0
		move.w #$0000,d1
		move.w d7,d3
		move.w #$0000,d7
		sub.w #$0001,d3		;Set BCD adder to 1
hex2decloop:
		cmpi.b #$99,d1		;are we overflowing?
		beq bcdoverflow	
		abcd d0,d1			;add binary coded decimal
bcd_ret:		
		dbf d3, hex2decloop	
		bra end_bcd
bcdoverflow:
		abcd d0,d7			;count number of times we hit 99
		move.w #$0000,d1	;clear first counter
		bra bcd_ret
end_bcd:
		lsl.w #$08,d7		;make some room for 8 bits
		eor.w d1,d7 		;combine overflow register with main register
		move.w (sp)+,d0		
		move.w (sp)+,d1		;pop registers
		move.w (sp)+,d3	
		rts	
		
sell_shit:
		moveq #$00000000,d0
		move.w totaliron,d0
		mulu.w #10,d0
		add.l d0,cash
		move.w totalcopper,d0
		mulu.w #10,d0
		add.l d0,cash
		move.w totalsilver,d0
		mulu.w #25,d0
		add.l d0,cash
		move.w totalgold,d0
		mulu.w #50,d0
		add.l d0,cash
		move.w totalemerald,d0
		mulu.w #100,d0
		add.l d0,cash
		move.w totaldarkmatter,d0
		mulu.w #250,d0
		add.l d0,cash
		rts
		
region_check:
		clr d0
        move.b  $A10001, d0
		andi.b #$40, d0
		cmpi.b #$40, d0
		 beq pal 
		bra ntsc
pal:
		move.b #$50, region	
		rts
ntsc:				
        move.b #$60, region	
		rts	

play_drill:
		;cmpi.b #$ff,sfx_flag
		;beq return ;less spammy
		move.w input,d0
		move.w oldinput,d1
		andi.b #$0f,d0
		andi.b #$0f,d1
		cmp.b d0,d1
		beq nosfx
		
		lea (sfx_drill),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
nosfx:
		move.w input,oldinput	
		rts

check_ore:
		cmpi.b #$94,d5
		beq addiron
		cmpi.b #$a6,d5
		beq addcopper
		cmpi.b #$9d,d5
		beq addsilver
		cmpi.b #$af,d5
		beq addgold
		cmpi.b #$b8,d5
		beq addemerald
		cmpi.b #$c1,d5
		beq adddarkmatter
		cmpi.b #$ca,d5
		beq addbattery
		rts
		
addiron:
		add.w #$01,totaliron
		lea (sfx_ore),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
		rts
addcopper:
		add.w #$01,totalcopper
		lea (sfx_ore),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
		rts		
addsilver:
		add.w #$01,totalsilver
		lea (sfx_ore),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
		rts
addgold:
		add.w #$01,totalgold
		lea (sfx_ore),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
		rts
addemerald:
		add.w #$01,totalemerald
		lea (sfx_ore),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
		rts
adddarkmatter:
		add.w #$01,totaldarkmatter
		lea (sfx_ore),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
		rts
addbattery:
		lea (sfx_ore),a0
		move.l a0,sfx_tmp
		move.b #$ff,sfx_flag
		rts
		
 include "titlescreen.asm"
 include "clock.asm"
 include "music_driver_V3.asm"
 include "decompress.asm"
 include "hex2bcdV2.asm"
 include "crashscreen v3.asm"
 include "data.asm"
 
 dc.b "What an ingenious device, boredom encompasses my time. I don't know what I should do. "

ROM_End:
              
              