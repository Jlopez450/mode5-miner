VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ; Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8210	;field A    
	dc.w $8300	;$833e	
	dc.w $8405	;field B	
	dc.w $857c	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B02	;h scroll mode	
	dc.w $8C81	
	dc.w $8D3c		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200
	
palettes:	
palette_dirt:
	dc.w $026A,$0026,$0048,$0028,$024A,$026A,$048C,$004A
	dc.w $046C,$026C,$0004,$0006,$0248,$0268,$046A,$068E
	
pallette_ore:
	dc.w $0000,$0000,$0246,$0468,$006C,$0AAA,$0666,$0EEE
	dc.w $0268,$004C,$0482,$00CC,$00EE,$0480,$06A2,$0888

pallette_player:
	dc.w $0E0E,$088A,$0AAA,$0000,$0888,$0666,$0668,$0224
	dc.w $000E,$00E0,$0E00,$0eee,$0eee,$0000,$0000,$0000
	
pallette_hud:
	dc.w $0E0E,$0000,$0222,$0CCC,$0AAA,$0000,$0002,$0006
	dc.w $000A,$0888,$0222,$000C,$000E,$0666,$0444,$00E0
	
pallette_bg:
	dc.w $006A,$006A,$0046,$0044,$0066,$0026,$0000,$0CCC
	dc.w $0024,$008A,$026C,$008C,$0AAA,$0E44,$044E,$0028
	
palette_title:
	dc.w $0842,$0842,$0a64,$0eee,$0000,$0EEE,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000
	
palette_titletext:
	dc.w $0000,$0000,$0000,$0000,$0842,$0000,$0000,$0000
	dc.w $0000,$0000,$0000,$0000,$0EEE,$0000,$0000,$0000
	
mapdata_sky:
	dc.w	$0000,$0001,$0002,$0003,$0004,$0005,$0006,$0007,$0008,$0009,$000A,$000B,$000C,$000D,$000E,$000F,$0010,$0011,$0012,$0013,$0014,$0015,$0016,$0017,$0018,$0019,$001A,$001B,$001C,$001D,$001E,$001E,$001F,$0020,$0021,$0022,$0023,$0024,$0025,$0026,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$0028,$0029,$002A,$002B,$002C,$002D,$002E,$002F,$0030,$001E,$0031,$0032,$0033,$0034,$0035,$0036,$0037,$0038,$0039,$003A,$003B,$003C,$003D,$003E,$003F,$0040,$0041,$0042,$0043,$0044,$0045,$0046,$0047,$0048,$0049,$004A,$004B,$004C,$004D,$004E,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$004F,$0050,$0051,$0052,$0053,$0054,$0055,$0056,$0057,$0058,$0059,$005A,$001E,$005B,$005C,$005D,$005E,$005F,$0060,$0061,$0062,$0063,$0064,$0065,$0066,$0067,$0068,$0069,$006A,$006B,$006C,$006D,$006E,$006F,$0070,$006E,$0071,$0072,$006E,$0073,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$0074,$0075,$0076,$0077,$0078,$0079,$007A,$007B,$007C,$007D,$007E,$007F,$0080,$0081,$0082,$0083,$0084,$0085,$0086,$0087,$0088,$0089,$008A,$008B,$008C,$008D,$008E,$008F,$0090,$0091,$0092,$0093,$0094,$0095,$0096,$0097,$0098,$0099,$009A,$009B,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$009C,$009D,$009E,$009F,$00A0,$00A1,$00A2,$00A3,$00A4,$00A5,$00A6,$00A7,$00A8,$00A9,$00AA,$00AB,$00AC,$00AD,$00AE,$00AF,$00B0,$00B1,$00B2,$00B3,$00B4,$00B5,$00B6,$00B7,$00B8,$00B9,$00BA,$00BB,$00BC,$00BD,$00BE,$00BF,$00C0,$00C1,$00C2,$00C3,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$00C4,$00C5,$00C6,$00C7,$00C8,$00C9,$00CA,$00CB,$00CC,$00CD,$00CE,$00CF,$00D0,$00D1,$00D2,$00D3,$00D4,$00D5,$00D6,$00D7,$00D8,$00D9,$00DA,$00DB,$00DC,$00DD,$00DE,$00DF,$00E0,$00E1,$00E2,$00E3,$00E4,$00E5,$00E6,$00E7,$00E8,$00E9,$00EA,$00EB,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$00EC,$00ED,$00EE,$00EF,$00F0,$00F1,$00F2,$00F3,$00F4,$00F5,$00F6,$00F7,$00F8,$00F9,$00FA,$00FB,$00FC,$00FD,$00FE,$00FF,$0100,$0101,$0102,$0103,$0104,$0105,$0106,$0107,$0108,$0109,$010A,$010B,$010C,$010D,$010D,$010D,$010D,$010D,$010E,$010F,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$0110,$0111,$0112,$0113,$0114,$0115,$0116,$0117,$0118,$0119,$011A,$011B,$011C,$011D,$001E,$011E,$011F,$0120,$0121,$0122,$0123,$0124,$0125,$0126,$0127,$0128,$0129,$012A,$012B,$012C,$012D,$012E,$012E,$012E,$012E,$012E,$012E,$012E,$012E,$012F,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$0130,$0131,$0132,$0133,$0134,$0135,$0136,$0137,$0138,$0139,$013A,$013B,$013C,$013D,$013E,$013F,$0140,$0141,$0142,$0143,$0144,$0145,$0146,$0147,$0148,$0149,$014A,$014B,$014C,$014D,$014E,$014F,$014F,$014F,$014F,$014F,$014F,$014F,$014F,$0150,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$0151,$0152,$0153,$0154,$0155,$0156,$0157,$0158,$0159,$015A,$015B,$015C,$015D,$015E,$015F,$0160,$0161,$0162,$0163,$0164,$0165,$0166,$0167,$0168,$0169,$016A,$016B,$016C,$016C,$016C,$016D,$016E,$016E,$016E,$016F,$0170,$0171,$016E,$016E,$0172,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$0173,$0174,$0175,$0176,$0177,$0178,$0179,$017A,$017B,$017C,$017D,$017E,$017F,$0180,$0181,$0182,$0183,$0184,$0185,$0186,$0187,$0188,$0189,$018A,$018B,$018C,$018D,$018E,$018E,$018E,$018F,$0190,$0190,$0191,$0192,$0193,$0194,$0195,$0190,$0196,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$0197,$0198,$0199,$019A,$019B,$019C,$019D,$019E,$019F,$01A0,$01A1,$01A2,$01A3,$01A4,$01A5,$01A6,$01A7,$01A8,$01A9,$01AA,$01AB,$01AC,$01AD,$01AE,$01AF,$01B0,$01B1,$01B2,$01B2,$01B2,$01B3,$0190,$0190,$01B4,$01B5,$01B6,$01B7,$01B8,$0190,$0196,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	dc.w	$01B9,$01BA,$01BB,$01BC,$01BD,$01BE,$01BF,$01C0,$01C1,$013B,$01C2,$01C3,$01C4,$01C5,$01C6,$01C7,$01C8,$01C9,$01CA,$01CB,$01CC,$01CD,$01CE,$01CF,$01D0,$01D1,$01D2,$01D3,$01D3,$01D3,$01D4,$01D5,$01D5,$01D5,$01D6,$01D7,$01D8,$01D5,$01D5,$01D9,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027,$0027
	
pcyc1:
	dc.w $000e, $00e0, $0e00
pcyc2:
	dc.w $0e00, $000e, $00e0
pcyc3:
	dc.w $00e0, $0e00, $000e
	
pausescreen:
 dc.b "                                        *"
 dc.b " -------------------------------------- *"
 dc.b " |To: Current Operator                | *"
 dc.b " |From: Mode 5 Mining Inc.            | *"
 dc.b " |Date: September 6, 2094             | *"
 dc.b " |Subject: Mining report              | *" 
 dc.b " |                                    | *"
 dc.b " |                                    | *"
 dc.b " |                                    | *"
 dc.b " |             Ore stored:            | *"
 dc.b " |                                    | *"
 dc.b " |       Iron:                        | *"
 dc.b " |     Copper:                        | *"
 dc.b " |     Silver:                        | *"
 dc.b " |       Gold:                        | *"
 dc.b " |   Emeralds:                        | *"
 dc.b " | DarkMatter:                        | *"
 dc.b " |                                    | *"
 dc.b " |              Upgrades:             | *"
 dc.b " |                                    | *"
 dc.b " | Battery type:                      | *"
 dc.b " |                                    | *"
 dc.b " |                Misc:               | *"
 dc.b " |                                    | *"
 dc.b " |      Credits:$                     | *"
 dc.b " |Surface warps:                      | *"
 dc.b " |____________________________________| *"
 dc.b "                                        %"
 
storescreen:
 dc.b "                                        *"
 dc.b "            Payment for ore:            *"
 dc.b "                                        *"
 dc.b "       Iron pay ($01 each): $XXXX       *"
 dc.b "     Copper pay ($02 each): $XXXX       *"
 dc.b "     Silver pay ($05 each): $XXXX       *"
 dc.b "       Gold pay ($10 each): $XXXX       *"
 dc.b "    Emerald pay ($25 each): $XXXX       *"
 dc.b " DarkMatter pay ($50 each): $XXXX       *"
 dc.b "                                        *"
 dc.b " Cash added:                            *"
 dc.b " Cash total:                            *"
 dc.b "                                        *"
 dc.b "                 Store:                 *"
 dc.b "                                        *"
 dc.b " Upgrade battery $XXXX                  *"
 dc.b " Current level:                         *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        %"
 
helpscreen:
 dc.b "                                        *"
 dc.b " What's happening?:                     *"
 dc.b "                                        *"
 dc.b " You are a drone operator contracted by *"
 dc.b " The Mode 5 mining company to conduct a *"
 dc.b " prospecting mission on planet Venus.   *"
 dc.b " Dig deep in to the planet to uncover   *"
 dc.b " the valuables buried within.           *"
 dc.b " Your J-3 mining probe is capable of    *"
 dc.b " operating in the deep depths of the    *"
 dc.b " planet. It's operated remotely and can *"
 dc.b " be upgraded... For a fee of course!    *"
 dc.b "                                        *"
 dc.b " Controls:                              *"
 dc.b "                                        *"
 dc.b " The J-3 mining probe can be controlled *"
 dc.b " using the d-pad. The radio can be      *"
 dc.b " tuned using the 'B' and 'C' buttons.   *"
 dc.b "                                        *"
 dc.b " Limitations:                           *"
 dc.b "                                        *"
 dc.b " The J-3 mining probe can dig in all    *"
 dc.b " four directions and roll horizontally. *"
 dc.b " However, it is only possible to dig up *"
 dc.b " if there is soil to dig through.       *"
 dc.b " Because of this, you must avoid        *"
 dc.b " trapping yourself underground.         *"
 dc.b "                                        %"
 
orescreen:
 dc.b "                                        *"
 dc.b " Venus contains a small variety of      *" 
 dc.b " valuable resources. Below is a list of *" 
 dc.b " ores, their value, and at what depth   *" 
 dc.b " they are found.                        *" 
 dc.b "                                        *" 
 dc.b " Battery:",$c6,$c7,$c8," Re-fills your energy.      *"
 dc.b "         ",$c9,$ca,$cb," Value: N/A                 *"
 dc.b "         ",$cc,$cd,$ce," Found below: Surface       *"
 dc.b "    Iron:",$90,$91,$92,"                            *"
 dc.b "         ",$93,$94,$95," Value: $10                 *"
 dc.b "         ",$96,$97,$98," Found below: Surface       *"
 dc.b "  Copper:",$a2,$a3,$a4,"                            *"
 dc.b "         ",$a5,$a6,$a7," Value: $10                 *"
 dc.b "         ",$a8,$a9,$aa," Found below: 100M          *"
 dc.b "  Silver:",$99,$9a,$9b,"                            *"
 dc.b "         ",$9c,$9d,$9e," Value: $25                 *"
 dc.b "         ",$9f,$a0,$a1," Found below: 250M          *"
 dc.b "    Gold:",$ab,$ac,$ad,"                            *"
 dc.b "         ",$ae,$af,$b0," Value: $50                 *"
 dc.b "         ",$b1,$b2,$b3," Found below: 500M          *"
 dc.b " Emerald:",$b4,$b5,$b6,"                            *"
 dc.b "         ",$b7,$b8,$b9," Value: $100                *"
 dc.b "         ",$ba,$bb,$bc," Found below: 750M          *"
 dc.b "     Dark",$bd,$be,$bf,"                            *"
 dc.b "  Matter:",$c0,$c1,$c2," Value: $250                *"
 dc.b "         ",$c3,$c4,$c5," Found below: Deep          *"
 dc.b "                                        %" 
 
creditscreen:
 dc.b "                                        *"
 dc.b " Music, programming, art, testing, etc. *"
 dc.b " by James Lopez.                        *"
 dc.b "                                        *"
 dc.b " Contact: ComradeOj@yahoo.com           *"
 dc.b "                                        *"
 dc.b " Also, thanks for checking out the      *"
 dc.b " credits! As a bonus you'll start the   *" ;ToDo: Give bonus for viewing credits
 dc.b " game some extra cash.                  *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "               Meta info:               *"
 dc.b "                                        *"
 dc.b " ROM size: 85,464 bytes                 *"
 dc.b " Build date: June 8, 2016               *"
 dc.b " Project start date: May 23, 2016       *"
 dc.b "                                        *"
 dc.b "                                        *"
 dc.b "                    Visit www.mode5.net *"
 dc.b "                                        %"
 
battlvl1:
 dc.b "Lead-acid (100Ah)$"
battlvl2:
 dc.b "Ni-Cad (250Ah)$ "
battlvl3:
 dc.b "Lithium (500Ah)$"
battlvl4:
 dc.b "Nuclear (750Ah)$"
battlvl5:
 dc.b "DarkMatter (999Ah)$ "
	
hudtext1:
 dc.b "Time-  : $"
hudtext2:
 dc.b "Depth-   M$ "
hudtext3:
 dc.b "Power-XXXAh$"
	
hud:
 incbin "gfx/hud3.bin"
 
font:
 incbin "gfx/font.bin"
 
sky:
 incbin "gfx/bg.kos"
 
bg_title:
 incbin "gfx/titlescreen.kos"
 
tiles:
 incbin "gfx/dirt2.bin"
 incbin "gfx/ore.bin"
 
sprites:
 ;incbin "gfx/player.bin"
 incbin "gfx/player2.bin"
 incbin "gfx/chute.bin"
 incbin "gfx/drill.bin"
 
music1:
 incbin "sound/music1.vgm"
music2:
 incbin "sound/music2.vgm"
music3:
 incbin "sound/music3.vgm"
music4:
 incbin "sound/music4.vgm"

sfx_ore:
 incbin "sound/sfx_ore.vgm"
sfx_drill:
 incbin "sound/sfx_drill.vgm"