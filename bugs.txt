pausing/unpausing while moving up/down can mis-alaign the player from the 3x3 grid. Sometimes causes infinite moving, as if holding down.
Ignores bottom limit.

doing the same moving side to side will break the game