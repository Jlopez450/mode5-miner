;8 Digit HEX to Decimal converter
;needs a 8 byte bcdout label in RAM.
;original code from Phantasy Star II

;usage example:
	; move.l #$dead,d0
	; lea bcdout,a1
	; bsr convert

convert:	
	movem.l d0/d2/d3/d4,-(sp)
	moveq #$00000000,d1
	move.l d1,d2
	move.l d1,d3
	move.l d1,d4
	lea	(DecimalConverReferArray).l, a2
	moveq	#7, d1		; eight digits

	moveq	#0, d4	; unset flag for digit start place
DrawDigitInVRAMLoop2:
	tst.b	d1
	bne.s	next			; branch if we are not in the one's place yet
	move.b	#1, d4		; if we get here, we are at the last iteration of the loop, thus placing the unit digit. We need to display at least the unit digit,
						; so set the flag no matter what.
next:
	moveq	#0, d2		; if d2 and d4 are 0, then the space character will be drawn
	move.l	(a2)+, d3
back:
	sub.l	d3, d0		; subtract the money we currently own with the range number
	bcs.s	next2			; if it's less than the owned money, branch
	addq.b	#1, d2		; advance digit counter in VRAM
	bra.s	back		; subtract again
next2:
	add.l	d3, d0		; re-add money to make it positive so we can process it with lower place value
	;add.b #$20, d2		;convert to ASCII
	move.b	d2, (a1)+	; pick character from digit counter
	dbf	d1, DrawDigitInVRAMLoop2	; next place value
	;rts ;uncomment for ASCII
fix: ;can skip this crap is converting directly to ASCII
	moveq.l #$00000000,d1
	moveq.l #$00000000,d2
	lea bcdout, a1
	move.b (a1)+,d2
	lsl.l #$08,d2
	lsl.l #$08,d2	;ToDo: replace with a SWAP instruction for more speed
	lsl.l #$06,d2	
	lsl.l #$06,d2	
	eor.l d2,d1
	moveq.l #$00000000,d2	
	move.b (a1)+,d2
	lsl.l #$08,d2
	lsl.l #$08,d2	
	lsl.l #$08,d2	
	eor.l d2,d1
	moveq.l #$00000000,d2	
	move.b (a1)+,d2
	lsl.l #$08,d2
	lsl.l #$08,d2	
	lsl.l #$04,d2	
	eor.l d2,d1
	moveq.l #$00000000,d2	
	move.b (a1)+,d2
	lsl.l #$08,d2
	lsl.l #$08,d2	
	eor.l d2,d1
	moveq.l #$00000000,d2	
	move.b (a1)+,d2
	lsl.l #$08,d2
	lsl.l #$04,d2	
	eor.l d2,d1
	moveq.l #$00000000,d2	
	move.b (a1)+,d2
	lsl.l #$08,d2
	eor.l d2,d1
	moveq.l #$00000000,d2	
	move.b (a1)+,d2
	lsl.l #$04,d2
	eor.l d2,d1	
	moveq.l #$00000000,d2	
	move.b (a1)+,d2
	eor.l d2,d1
	movem.l (sp)+,d0/d2/d3/d4
	rts

; ====================================
DecimalConverReferArray:
	dc.l	$989680 ; 10000000
	dc.l	$F4240 	; 1000000
	dc.l	$186A0	; 100000
	dc.l	$2710 	; 10000
	dc.l	$3E8 	; 1000
	dc.l	$64		; 100
	dc.l	$A		; 10
	dc.l	1 		; 1
; ====================================	