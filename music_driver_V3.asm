music_driver:
		; clr d6
		; cmpi.b #$ff,sfx_flag ;slow and delayed
		; beq play_sfx		
vgm_loop:
		bsr test2612	
        move.b (a2)+,d6
		cmpi.b #$61,d6
		 beq wait		
		cmpi.b #$66,d6
		 beq loop_playback 	
		cmpi.b #$52,d6 
		 beq update2612_0
		cmpi.b #$53,d6 
		 beq update2612_1
		cmpi.b #$50,d6
		 beq update_psg

		cmpi.b #$ff,sfx_flag  ;fixed?
		beq play_sfx	
sfx_ret:
		bra vgm_loop
	
update2612_0:
        move.b (a2)+,$A04000
		nop
        move.b (a2)+,$A04001
        bra vgm_loop
		
update2612_1:	
	    move.b (a2)+,$A04002
		nop
        move.b (a2)+,$A04003
		bra vgm_loop
		
loop_playback:	
		move.l vgm_start,a2
		rts
		
update_psg:
        move.b (a2)+,$C00011
		bra vgm_loop
	
wait:
		rts
		
test2612:
		clr d6
        move.b $A04001,d6
		andi.b #$80,d6
        cmpi.b #$80,d6
		beq test2612 
		rts		
		
play_sfx:
		move.l a2,-(sp)
		move.l sfx_tmp,a2
sfx_loop:
		bsr test2612		
        move.b (a2)+,d6		
		cmpi.b #$61,d6
		 beq end_sfx
		cmpi.b #$66,d6
		 beq no_sfx 	;normally loop command	
		cmpi.b #$52,d6 
		 beq sfx_update2612_0
		cmpi.b #$53,d6 
		 beq sfx_update2612_1		 
end_sfx:		 
		move.l a2,sfx_tmp
		move.l (sp)+,a2
		bra sfx_ret
no_sfx:	
		move.b #$00,sfx_flag
		move.l (sp)+,a2
		bra sfx_ret

sfx_update2612_0:
        move.b (a2)+,$A04000
		nop
        move.b (a2)+,$A04001
        bra sfx_loop
		
sfx_update2612_1:	
	    move.b (a2)+,$A04002
		nop
        move.b (a2)+,$A04003
		bra sfx_loop